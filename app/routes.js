const { exchangeRates } = require('../src/utils.js');

module.exports = (app) => {
	app.get('/', (req, res) => {
		return res.send({data: {} })
	});

	app.get('/currency', (req, res) => {
		return res.send({
			usd: exchangeRates
		})
	});

	app.post('/currency', (req, res) => {
		if(!req.body.hasOwnProperty('name')){
			return res.status(400).send({
				'error' : 'Bad Request : missing required parameter NAME'
			})
		}
		
		if(typeof req.body.name !== 'string'){
			return res.status(400).send({
				'error' : 'Bad Request : NAME has to be string' 
			})
		}
		if(typeof req.body.name === ""){
			return res.status(400).send({
				'error' : 'Bad Request : NAME should not be empty string' 
			})
		}
		
		if(typeof req.body.ex === ""){
			return res.status(400).send({
				'error' : 'Bad Request : missing required parameter ex'
			})
		}
		if(typeof req.body.ex !== 'number'){
			return res.status(400).send({
				'error' : 'Bad Request: Ex has to be a number'
			})
		}

		if(typeof req.body.ex === "Object") {
			return res.status(400).send({
				'error' : 'Bad Request : missing required parameter Object'
			})
		}
		if(typeof req.body.ex !== "Object") {
			return res.status(400).send({
				'error' : 'Bad Request : parameter is not an Object'
			})
		}


		if(!req.body.hasOwnProperty('alias')){
			return res.status(400).send({
				'error' : 'Bad Request : missing required parameter NAME'
			})
		}
		if(typeof req.body.alias !== 'string'){
			return res.status(400).send({
				'error' : 'Bad Request: Alias has to be a string'
			})
	}
	if(typeof req.body.alias === ''){
			return res.status(400).send({
				'error' : 'Bad Request: Alias is empty string'
			})
	}
})
}