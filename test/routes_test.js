const chai = require('chai')
const expect = chai.expect
// or const { expect } = require('chai')
const http = require('chai-http')

chai.use(http);

describe('API Test Suite', () => {
	it('Test API currency is running', () => {
		chai.request('http://localhost:5001').get('/currency')
		.end((err, res) => {
			expect(res).to.not.equal(undefined)
		})
	})

	it('Test API post currency returns 400 if name is missing', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			ex: "Philippine peso",
		
		})
		.end((err, res) => {
			console.log(res.status)
			expect(res.status).to.equal(400)
			
		})
		done();

	})

	it('Check if currency returns status 400 if name is not a string', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			name: "",
		
		})
		.end((err, res) => {
			// console.log(res.status);
			expect(res.status).to.equal(400);
		})
		done();
	})

	it('Test API post currency returns status 400 if name is empty string', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			name:"",
	
		})
		.end((err, res) => {
			expect(res.status).to.equal(400)
			
		})
		done();
	})

	it('Test API post currency returns status 400 if ex is missing', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			name: 'United States Dollar',
			
			
		})
		.end((err, res) => {
			expect(res.status).to.equal(400)
			
		})
		done();
	})

	it('Test API post currency returns status 400 if not an object', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send(
			"abc")
		.end((err, res) => {
			expect(res.status).to.equal(400)
			
		})
		done();
	})
	it('Test API post currency returns status 400 if ex is empty object', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({

		})
		.end((err, res) => {
			expect(res.status).to.equal(400)
			
		})
		done();
	})
	it('Test API post currency returns status 400 if alias is missing', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			name: 'United States Dollar'
		})
		.end((err, res) => {
			expect(res.status).to.equal(400)
			
		})
		done();
	})
	it('Test API post currency returns status 400 if alias is not a string', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			alias: '123'
		})
		.end((err, res) => {
			expect(res.status).to.equal(400)
			
		})
		done();
	})
	it('Test API post currency returns status 400 if alias is empty string', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			alias: ''
		})
		.end((err, res) => {
			expect(res.status).to.equal(400)
			
		})
		done();
	})
	it('Test API post currency returns status 400 if all fields are complete but there is a duplicate alias', (done) => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			alias: ''
		})
		.end((err, res) => {
			expect(res.status).to.equal(400)
			
		})
		done();
	})

})